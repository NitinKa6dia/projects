#!/bin/sh

export CONFIGURATION="release"
export SCHEME="Projects"
export PROJECT_DIR=$(pwd)"/Projects/Projects.xcodeproj"
export EXPORTOPTIONS_PATH=$(pwd)"/Projects/Scripts/ExportOptions.plist"
export CONFIG_PATH=$(pwd)"/Projects/Configurations/Development.xcconfig"

export IPAS_PATH=$(pwd)"/Output/IPA"
export CURRENT_DATE_TIME="`date +%Y%m%d%H%M%S`"
export PATH=$(pwd)"/Archive"
export ARCHIVE_PATH=$PATH"/Projects.xcarchive"
export EXPORT_PATH=$PATH"/"$CURRENT_DATE_TIME

#!/bin/bash
/bin/rm -rf $PATH
/bin/mkdir -p $PATH

/usr/bin/xcodebuild clean -project $PROJECT_DIR -configuration CONFIGURATION -alltragets

/usr/bin/xcodebuild archive -project $PROJECT_DIR -xcconfig $CONFIG_PATH -scheme $SCHEME -archivePath $ARCHIVE_PATH

/usr/bin/xcodebuild -exportArchive -archivePath $ARCHIVE_PATH -exportPath $EXPORT_PATH -exportOptionsPlist $EXPORTOPTIONS_PATH
