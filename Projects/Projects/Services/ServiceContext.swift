
import UIKit

class ServiceContext: NSObject {

    class var context: ServiceContext {
        struct Static {
            static let instance = ServiceContext()
        }
        return Static.instance
    }
    
    var userId = ""
    var userName = ""
}
