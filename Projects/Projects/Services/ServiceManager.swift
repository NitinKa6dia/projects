
import UIKit
import SystemConfiguration

typealias CompletionHandler = (_ error:Error?, _ response:Any?) -> Void

class ServiceManager: NSObject {

    private let baseURL:String = ""
    
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, { pointer in
            return pointer.withMemoryRebound(to: sockaddr.self, capacity: MemoryLayout<sockaddr>.size) {
                return SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else { return false }
        
        var flags : SCNetworkReachabilityFlags = []
        
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            
            return false
            
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    func showAlertWithTitle(_ title:String, error:String, view:UIViewController) {
        
        let alertOk:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        let alert:UIAlertController = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(alertOk);
        view.present(alert, animated: true, completion: nil)
    }
    func showAlertMessagge(_ error:String, view:UIViewController) {
        
        let alertOk:UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        let alert:UIAlertController = UIAlertController(title: "Alert", message: error, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(alertOk);
        view.present(alert, animated: true, completion: nil)
    }
    func somthingWentWrong() -> Error {
        
        let userInfo: [AnyHashable : Any] =
            [
                NSLocalizedDescriptionKey :  "Somthing went wrong",
                NSLocalizedFailureReasonErrorKey : ""
        ]
        
        let errorTemp = NSError(domain:"", code:100, userInfo:userInfo as? [String : Any])
        return errorTemp;
    }
    
    /*func Login(username:String, password:String, complition:@escaping CompletionHandler) {
        
        var params:String = ""
        params = params.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let url:String = "\(baseURL)\(params)"
        
        request(url).responseJSON { response in
            
            DispatchQueue.main.async {
                
                if let error = response.error {
                    
                    complition(error, nil)
                } else if let json = response.result.value {
                    
                    if let data = (json as AnyObject).object(forKey: "DATA") {
                        
                        let responseArray:NSArray = data as! NSArray
                        
                        if (responseArray.count > 0) {
                            
                            let dict:NSDictionary = responseArray.object(at: 0) as! NSDictionary;
                            let result:String = dict.object(forKey: ServiceResponseType.result) as? String ?? ""
                            
                            if result == ServiceResponseType.fail {
                                
                                let msg:String = dict.object(forKey: ServiceResponseType.msg) as? String ?? ""
                                let userInfo: [AnyHashable : Any] =
                                    [
                                        NSLocalizedDescriptionKey :  msg,
                                        NSLocalizedFailureReasonErrorKey : ""
                                ]
                                
                                let errorTemp = NSError(domain:"", code:100, userInfo:userInfo as? [String : Any])
                                complition(errorTemp, nil)
                            } else {
                                
                                complition(nil, responseArray.object(at: 0))
                            }
                        } else {
                         
                            complition(self.somthingWentWrong(), nil)
                        }
                        
                    } else {
                        
                        complition(self.somthingWentWrong(), nil)
                    }
                } else {
                    
                    complition(self.somthingWentWrong(), nil)
                }
            }
        }
    }
    
    func UploadFile(userId:String, priority:String, fileName:String, recordedDate:String, data:Data, complition:@escaping CompletionHandler) {

        let dateFirst = NSDate()
        var params:String = ""
        params = params.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let url:String = "\(baseURL)\(params)"
        var urlRequest = File().Upload(url: url, parameters: params, fileName: fileName, data: data, key: ServiceParams.File)
        urlRequest.timeoutInterval = 30*60;
        
        request(urlRequest).responseJSON { response in
            
            print("uploadedTime:\(Date().timeIntervalSince(dateFirst as Date))");
            DispatchQueue.main.async {
                
                if let error = response.error {
                    
                    complition(error, nil)
                } else if let json = response.result.value {
                    
                    if let data = (json as AnyObject).object(forKey: "DATA") {
                        
                        let responseArray:NSArray = data as! NSArray
                        
                        if (responseArray.count > 0) {
                            
                            let dict:NSDictionary = responseArray.object(at: 0) as! NSDictionary;
                            let result:String = dict.object(forKey: ServiceResponseType.result) as? String ?? ""
                            
                            if result == ServiceResponseType.fail {
                                
                                let msg:String = dict.object(forKey: ServiceResponseType.msg) as? String ?? ""
                                let userInfo: [AnyHashable : Any] =
                                    [
                                        NSLocalizedDescriptionKey :  msg,
                                        NSLocalizedFailureReasonErrorKey : ""
                                ]
                                
                                let errorTemp = NSError(domain:"", code:100, userInfo:userInfo as? [String : Any])
                                complition(errorTemp, nil)
                            } else {
                                
                                complition(nil, responseArray.object(at: 0))
                            }
                        } else {
                            
                            complition(self.somthingWentWrong(), nil)
                        }
                        
                    } else {
                        
                        complition(self.somthingWentWrong(), nil)
                    }
                } else {
                    
                    complition(self.somthingWentWrong(), nil)
                }
            }
        }
    }*/
}

class File: NSObject {
    
    func Upload(url:String, parameters:String, fileName:String,data:Data,key:String) -> URLRequest {
        
        let keyValues = parameters.components(separatedBy: "&");
        let params = NSMutableDictionary()
        for index in 0..<keyValues.count {
            
            let str:String = keyValues[index];
            let keyValue:[String] = str.components(separatedBy: "=")
            if keyValue.count > 1 {
                
                params.setValue(keyValue[1], forKey:keyValue[0]);
            }
        }
        
        
        var r  = URLRequest(url: URL(string: url)!)
        r.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        r.httpBody = createBody(parameters: params,
                                boundary: boundary,
                                data: data,
                                mimeType: "audio*",
                                filename: fileName,
                                key: key)
        return r;
    }
    func createBody(parameters: NSDictionary, boundary: String, data: Data, mimeType: String, filename: String, key:String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("\r\n")
        body.appendString("--".appending(boundary.appending("--")))
        
        return body as Data
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
