
import UIKit
import CoreData

class DataStore: NSObject {

    func save(data: Data) {

        let managedContext =
            CoreDataStack.managedObjectContext
        let entity =
            NSEntityDescription.entity(forEntityName: "Audio", in: managedContext)!
        
        let audio = NSManagedObject(entity: entity, insertInto: managedContext)
        audio.setValue(data, forKeyPath: "data")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    /*func AudioList(date:String) -> [HistoryModel] {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Audio")
        var history:[HistoryModel] = []
        
        do {
            let people = try managedContext.fetch(fetchRequest)
            
            for (_, element) in people.enumerated() {

                let model:HistoryModel =  NSKeyedUnarchiver.unarchiveObject(with: element.value(forKey: "data") as! Data) as! HistoryModel
                
                if (date == model.created.toString(dateFormat: "ddMMyyyy") && ServiceContext.context.userName == model.userName) {
                    
                    history.append(model)
                }
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return history;
    }
    func AudioAllList() -> [HistoryModel] {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Audio")
        var history:[HistoryModel] = []
        
        do {
            let people = try managedContext.fetch(fetchRequest)
            
            for (_, element) in people.enumerated() {
                
                let model:HistoryModel =  NSKeyedUnarchiver.unarchiveObject(with: element.value(forKey: "data") as! Data) as! HistoryModel
                
                if (ServiceContext.context.userName == model.userName) {
                    
                    history.append(model)
                }
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return history;
    }
    
    func deleteAudio(history:HistoryModel) {
        
        let managedContext =
            CoreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Audio")
        
        do {
            let people = try managedContext.fetch(fetchRequest)
            for (_, element) in people.enumerated() {
                
                let model:HistoryModel =  NSKeyedUnarchiver.unarchiveObject(with: element.value(forKey: "data") as! Data) as! HistoryModel
                if model.file == history.file &&
                   model.created == history.created {
                    managedContext.delete(element)
                }
            }
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let destination = documentsDirectory.appendingPathComponent(history.file)
            CoreDataStack.saveContext()
            
            do {
                
                _ = try FileManager.default.removeItem(at: destination)
            }catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }*/
}
